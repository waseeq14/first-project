import express from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";
import UserRouter from "./api/user.routes.js";

dotenv.config();

const app = express();
app.use(express.json());

await mongoose
  .connect(process.env.MONGODBURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("connected to mongoDB"))
  .catch((err) => console.log("Error connecting to MongoDB", err));

app.get("/", (req, res) => {
  res.send("HELLOO");
});

app.use("/users", UserRouter);

const port = 5000;

app.listen(port, () => {
  console.log(`server is running on port, ${port}`);
});
